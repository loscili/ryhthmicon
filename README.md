# Ryhthmicon

A virtual rhythmicon instrument created within PureData, an open source visual programming language for multimedia. See the readme for more information and links.

PureData information and download: https://puredata.info/
Wikipedia entry about the Rhythmicon instrument: https://en.wikipedia.org/wiki/Rhythmicon
For an animated description of the instrument, I recommend Nikolas Slonimsky's interview in the documentary, Thermin: An Electronic Odyssey: https://en.wikipedia.org/wiki/Theremin:_An_Electronic_Odyssey 

Lastly, a youtube video of this virtual rhythmicon in action is here: https://youtu.be/762nqjPpRoQ

To run this program:
The ryhtmicon runs in the PureData enviornment. You will need to download and install from the https://puredata.info website.
Downlaod this repository to a working directory on your system. All files are included and run within the repository folder. No other dependencies are needed.

When loading the rhythmicon this is the screen we are presented with. User selects the base frequency that all of the overtones are derived from. This instrument can be connected to a midi keyboard as input device, or used solely with mouse input. 

![Startup Screen](readmeImages/Rhythmicon_7_startWindow.png?raw=true "Startup Screen")

Clicking on the box marked "rhythmGrid.2" brings up the main interactive interface. Here, the user selects the rhythmic placement of a frequency within the harmonic framework of the chosen base frequency. And can set the tempo of the "rotating disk".

![Startup Screen](readmeImages/BetterSoundResult_coloredGrid.png?raw=true "Main interface")

The operators that make the sine waves are held within the MidiReceiver Object from the startup window.

![Startup Screen](readmeImages/MidiReceiver_holdsAllTheOperators.png?raw=true "Midi Receiver")

The operators are individual instances of the Op class

![Startup Screen](readmeImages/IndividualOperator.png?raw=true "Midi Receiver")
 
